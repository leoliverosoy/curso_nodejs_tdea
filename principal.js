const fs = require('fs');
const express = require('express')
const cursos = require('./cursos');
const app = express()
const yargs = require('yargs');

const opciones = {
    id: {
        describe: "Id del curso",
        alias : "i" 
    },
    nombre: {
        describe : "Nombre de persona",
        alias : "n"
    },
    cedula:{
        describe :"Cedula",
        alias : "c"
    }
};

const argv = yargs
    .command("inscribir","Inscribir curso",opciones)
    .help()
    .argv;

function esperar(i){
    setTimeout(()=> {
        console.log(cursos[i]);
    },2000*i)
}
    
function listado_cursos(cursos) {
    for (i = 0; i <= cursos.length -1; i++){
        esperar(i);}
}

if (argv._[0] == undefined){

    listado_cursos(cursos);
}
else{
    let asignatura = cursos.find(notaEst => notaEst.id == argv.id)
    if (asignatura == undefined){
        app.get('/', function (req, res) {
            res.send('No se ha encontrado ningun curso asociado con el Id =' + argv.id)
          })
        //console.log('No se ha encontrado ningun curso asociado con el Id =' + argv.id);
    }else{
        text = argv.nombre + ','+ argv.cedula + ',' + asignatura.Nombre + ',' + asignatura.id + ',' + asignatura.Valor + ',' + asignatura.Duracion + '\n';
        app.get('/', function (req, res) {
            res.send(text)
          })
        /*console.log(asignatura);
        fs.appendFile('Inscribir.txt', text, (err) => {
            if (err) throw err;
            console.log('Su inscripsion ha sido procesada con exito');
          });*/
    }
}

app.listen(3000, function() {
    // eslint-disable-next-line
    console.log('Server running on port: %d', 3000);
  })
//console.log(argv)
